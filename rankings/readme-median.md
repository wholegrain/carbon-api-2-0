How to generate the mean value for tests

1. Generate a new SQL table with the most recent tests for each unique url, for
the time period you are interested in.

This can be achieved with the following SQL statements

---

DROP TABLE IF EXISTS results;

CREATE TABLE `results` (
  `co2` decimal(18,16) DEFAULT NULL
);

INSERT into results
SELECT co2
FROM record
WHERE id IN (
    SELECT MAX(id)
    FROM record
    GROUP BY url
) AND timestamp > '2020-01-01 00:00:00'
ORDER BY co2 ASC;

SELECT AVG(dd.co2) as median_val
FROM (
SELECT d.co2, @rownum:=@rownum+1 as `row_number`, @total_rows:=@rownum
  FROM results d, (SELECT @rownum:=0) r
  ORDER BY d.co2
) as dd
WHERE dd.row_number IN ( FLOOR((@total_rows+1)/2), FLOOR((@total_rows+2)/2) );
